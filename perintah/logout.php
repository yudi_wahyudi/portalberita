<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
session_start(); //memulai membaca sesi
unset($_SESSION['id_anggota']); //menghapus nilai sesi yang diregistrasi
unset($_SESSION['nama_lengkap']); //menghapus nilai sesi yang diregistrasi
unset($_SESSION['login']); //menghapus nilai sesi yang diregistrasi
unset($_SESSION['loginadmin']);
unset($_SESSION['id_admin']);
session_destroy(); //menghancurkan sesi
header('location:../'); //mendirect ke index.php
?>
</body>
</html>